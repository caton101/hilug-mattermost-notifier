# HiLUG Mattermost Notifier
# Copyright (C) 2020  Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import slack
import asyncio
import time
from random import choice

MATTERMOST_MESSAGE = "Hey! We moved to Mattermost. https://chat.boonelinux.com/"

loop = asyncio.get_event_loop()

TIME_LAST_SCAN = time.time()
WAIT_TIME = 10

print("Starting Slack connection...")
client = slack.WebClient(
    token=os.environ['SLACK_API_TOKEN'],
    run_async=True
)

print("Getting channel list...")
channels = [[x["id"], x["name"]] for x in loop.run_until_complete(
    client.conversations_list()).data["channels"]]
print("Setting channel timestamps...")
channelClock = [time.time() for _ in range(len(channels))]

print("Joining channels...")
for channel in channels:
    try:
        print("\tTrying to join %s..." % channel[1])
        loop.run_until_complete(client.conversations_join(channel=channel[0]))
    except Exception as e:
        print(e)
        continue

def main():
    global TIME_LAST_SCAN
    while True:
        print("Scanning for messages...")
        indexCounter = 0
        for channel in channels:
            print("\tReading channel %s..." % channel[1])
            try:
                hist = loop.run_until_complete(
                    client.conversations_history(channel=channel[0])).data["messages"]
                hist = sorted(hist, key=lambda x: x['ts'])
                for message in hist:
                    ts = message["ts"]
                    if float(ts) > channelClock[indexCounter] and len(message['text']) > 0:
                        try:
                            loop.run_until_complete(client.chat_postMessage(
                                channel=message['user'],
                                text=MATTERMOST_MESSAGE
                            ))
                        except KeyboardInterrupt:
                            exit(0)
                        except Exception as e:
                            print(e)
                channelClock[indexCounter] = time.time()
                indexCounter += 1
                time.sleep(0.5)
            except KeyboardInterrupt:
                exit(0)
            except Exception as e:
                print(e)
        print("Scan complete.")
        print("Waiting for messages (%.2f seconds)..." % WAIT_TIME)
        time.sleep(WAIT_TIME)


try:
    main()
except KeyboardInterrupt:
    exit(0)
